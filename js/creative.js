/*!
 * Start Bootstrap - Creative Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    })

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    $('div.team-member-box').mouseenter(function(e) {
        $(this).find('img')
            .addClass('brighten-low');
        $(this).find('.caption')
            .removeClass('text-faded')
            .addClass('text-white');
    });
    $('div.team-member-box').mouseleave(function(e) {
        $(this).find('img')
            .removeClass('brighten-low');
        $(this).find('.caption')
            .removeClass('text-white')
            .addClass('text-faded');
    });

    // Fit Text Plugin for Main Header
    $("header h1").fitText(
        1.2, {
            minFontSize: '35px',
            maxFontSize: '80px'
        }
    );
    $(".modal.modal-fullscreen h1").fitText(
        1.2, {
            minFontSize: '35px',
            maxFontSize: '60px'
        }
    );

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    // Parallax scrolling
    $('#inspiration').parallax({
        speed: 0.15
    });
    $('#dog').parallax({
        speed: 0.25
    });

    $("#owl-clients").owlCarousel({
        autoPlay: 3000,
        items : 5,
        lazyLoad : true,
        navigation : false,
        pagination : false
    });

    $('#productModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var product = button.data('product');
        setTimeout(function() {
            $(window).trigger('resize');
        }, 200);
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        var product_desc = "Lorem ipsum dolor sit amet, consectetur"
            +"adipiscing elit. Sed odio neque, mollis ullamcorper venenatis quis, pharetra eu turpis."
            +"Aenean molestie mattis egestas. Donec at arcu ultrices, ullamcorper felis sed, convallis"
            +"nunc. Proin faucibus viverra luctus. Etiam interdum iaculis urna. Nam quis ex at massa"
            +"condimentum mattis quis semper purus. Morbi ac ipsum quis purus ullamcorper lacinia eu"
            +"nec orci.";
        if (product == 'cmp') {
            modal.find('.product-title').text('Colorado Metallurgical Pathways');
            modal.find('img').attr('src', 'img/portfolio/colorado_metallurgical_pathways.jpg');
            product_desc = "Colorado Metallurgical Pathways allows high school and college students to "
                +"engage in the wonderful tradecraft of metallurgy. Forging, smelting, and improvement science "
                +"are just a few things that these students learn throughout this pathway course. Students are given "
                +"their own forge, plus access to grinders, sanders, a power hammer, a CNC, and endless numbers of PDSAs. "
                +"The mayor of Denver calls this program \"A revolution in teaching\".";
        }
        else if (product == 'saic') {
            modal.find('.product-title').text('Secret Agent Improvement Community');
            modal.find('img').attr('src', 'img/portfolio/secret_agent_improvement_community.jpg');
            product_desc = "Carnegie Labs sought to solve the biggest problem of the intelligence industry: The "
                +"lack of a Networked Improvement Community. SAIC brings together spies, state department officials, "
                +"and congressional members from around the world to facilitate collaboration, learning and improvement. "
                +"SAIC has been responsible for a number of intelligence successes, including the hacking of the CIA "
                +"database and the theft of North Korea's nuclear launch codes.";
        }
        else if (product == 'aqt') {
            modal.find('.product-title').text('Advancing Quantum Tunneling');
            modal.find('img').attr('src', 'img/portfolio/advancing_quantum_tunneling.jpg');
            product_desc = "The 2016 National Energy Directive, put forth by the Deparment of Energy, calls fusion power "
                +"\"The next big step in energy production\". Our sun produces 184 trillion Joules of energy per second; if "
                +"we were able to generate a fraction of that energy, we could power the world's energy grid for years to "
                +"come. AQT brings together top physicists to propel our knowledge of quantum tunneling, an important process "
                +"of nuclear fusion. Recent findings and theories generated by AQT have allowed our fusion technology to "
                +"generate power at 56% efficiency.";
        }
        else if (product == 'hen') {
            modal.find('.product-title').text('Heuristic Encephalopathy Network');
            modal.find('img').attr('src', 'img/portfolio/heuristic_encephalopathy_network.jpg');
            product_desc = "With the exception of heart disease and diabetes, brain disorders and diseases are the biggest "
                +"issues affecting the health of the United States. With grants from both the National Science Foundation and "
                +"the European Science Foundation, Carnegie Labs has started the Heuristic Encephalopathy Network, or HEN. "
                +"Utilizing the latest in heuristic learning and the power of improvement, Carnegie Labs and the members of "
                +"HEN hope to make leaps and bounds in the study of brain maladies.";
        }
        else if (product == 'xop') {
            modal.find('.product-title').text('Xenon Oxidation Project');
            modal.find('img').attr('src', 'img/portfolio/xenon_oxidation_project.jpg');
            product_desc = "The production and use of xenon gas has seen a sizeable increase in recent years. Xenon gas "
                +"has seen use in lamps, lasers, and even as a general anesthetic. A recent development in science has "
                +"revealed xenon's use as a powerful oxidation agent, and even as an explosive. Working closely with INTERPOL, "
                +"the FBI, and Canadian intelligence, Carnegie Labs is helping to seek out illegal XeO<sub>3</sub> "
                +"(xenon trioxide) and XeO<sub>4</sub> (xenon tetroxide) manufacturing labs around the United States, Canada, "
                +"and Europe. Rooting out these illegal operations helps science stay a peaceful mission.";
        }
        else if (product == 'pb') {
            modal.find('.product-title').text('Post-Baccalaureate Program');
            modal.find('img').attr('src', 'img/portfolio/post_baccalaureate_program.jpg');
            product_desc = "The crowning achievement of Carnegie Labs, the Post-Baccalaureate Program has helped thousands "
                +"of recent graduates become the best of the best. Graduates of the Post-Bac Program have landed jobs at "
                +"top companies around the world, including Google, Nike, and Squarespace. Never fear that your skills are "
                +"insignificant after becoming a Post-Bac. To be a Post-Bac means to have complete job security.";
        }
        modal.find('.product-description').html(product_desc);
    });

    // Initialize WOW.js Scrolling Animations
    new WOW().init();

})(jQuery); // End of use strict
